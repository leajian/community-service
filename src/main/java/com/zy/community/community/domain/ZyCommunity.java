package com.zy.community.community.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;

/**
 * 小区 对象 zy_community
 *
 * @author yangdi
 * @date 2020-12-10
 */
public class ZyCommunity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 小区id
     */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    /**
     * 小区名称
     */
    @Excel(name = "小区名称")
    private String communityName;

    /**
     * 小区编码
     */
    @Excel(name = "小区编码")
    private String communityCode;

    /**
     * 省区划码
     */
    @Excel(name = "省区划码")
    private String communityProvenceCode;

    /**
     * 市区划码
     */
    @Excel(name = "市区划码")
    private String communityCityCode;

    /**
     * 区县区划码
     */
    @Excel(name = "区县区划码")
    private String communityTownCode;

    /**
     * 详细地址
     */
    @Excel(name = "详细地址")
    private String communityDetailedAddress;

    /**
     * 经度
     */
    @Excel(name = "经度")
    private String communityLongitude;

    /**
     * 纬度
     */
    @Excel(name = "纬度")
    private String communityLatitude;

    /**
     * 物业id
     */
    @Excel(name = "物业id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long deptId;

    /**
     * 排序
     */
    @Excel(name = "排序")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communitySort;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getCommunityCode() {
        return communityCode;
    }

    public void setCommunityCode(String communityCode) {
        this.communityCode = communityCode;
    }

    public String getCommunityProvenceCode() {
        return communityProvenceCode;
    }

    public void setCommunityProvenceCode(String communityProvenceCode) {
        this.communityProvenceCode = communityProvenceCode;
    }

    public String getCommunityCityCode() {
        return communityCityCode;
    }

    public void setCommunityCityCode(String communityCityCode) {
        this.communityCityCode = communityCityCode;
    }

    public String getCommunityTownCode() {
        return communityTownCode;
    }

    public void setCommunityTownCode(String communityTownCode) {
        this.communityTownCode = communityTownCode;
    }

    public String getCommunityDetailedAddress() {
        return communityDetailedAddress;
    }

    public void setCommunityDetailedAddress(String communityDetailedAddress) {
        this.communityDetailedAddress = communityDetailedAddress;
    }

    public String getCommunityLongitude() {
        return communityLongitude;
    }

    public void setCommunityLongitude(String communityLongitude) {
        this.communityLongitude = communityLongitude;
    }

    public String getCommunityLatitude() {
        return communityLatitude;
    }

    public void setCommunityLatitude(String communityLatitude) {
        this.communityLatitude = communityLatitude;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getCommunitySort() {
        return communitySort;
    }

    public void setCommunitySort(Long communitySort) {
        this.communitySort = communitySort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("communityId", getCommunityId())
                .append("communityName", getCommunityName())
                .append("communityCode", getCommunityCode())
                .append("communityProvenceCode", getCommunityProvenceCode())
                .append("communityCityCode", getCommunityCityCode())
                .append("communityTownCode", getCommunityTownCode())
                .append("communityDetailedAddress", getCommunityDetailedAddress())
                .append("communityLongitude", getCommunityLongitude())
                .append("communityLatitude", getCommunityLatitude())
                .append("communityDeptId", getDeptId())
                .append("communitySort", getCommunitySort())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
