package com.zy.community.community.domain.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * @author yangdi
 */
public class ZyRoomDto extends BaseEntity {

    /** 房间id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roomId;

    /** 小区id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    private String communityName;

    /** 楼栋id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long buildingId;

    private String buildingName;

    /** 单元id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long unitId;

    private String unitName;

    /** 楼层 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roomLevel;

    /** 房间编号 */
    private String roomCode;

    /** 房间名称 */
    private String roomName;

    /** 房屋建筑面积 */
    private BigDecimal roomAcreage;

    /** 算费系数 */
    private BigDecimal roomCost;

    /** 房屋状态 */
    private String roomStatus;

    /** 是否商铺 */
    private String roomIsShop;

    /** 是否商品房 */
    private String roomSCommercialHouse;

    /** 房屋户型 */
    private String roomHouseType;

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Long getRoomLevel() {
        return roomLevel;
    }

    public void setRoomLevel(Long roomLevel) {
        this.roomLevel = roomLevel;
    }

    public String getRoomCode() {
        return roomCode;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public BigDecimal getRoomAcreage() {
        return roomAcreage;
    }

    public void setRoomAcreage(BigDecimal roomAcreage) {
        this.roomAcreage = roomAcreage;
    }

    public BigDecimal getRoomCost() {
        return roomCost;
    }

    public void setRoomCost(BigDecimal roomCost) {
        this.roomCost = roomCost;
    }

    public String getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(String roomStatus) {
        this.roomStatus = roomStatus;
    }

    public String getRoomIsShop() {
        return roomIsShop;
    }

    public void setRoomIsShop(String roomIsShop) {
        this.roomIsShop = roomIsShop;
    }

    public String getRoomSCommercialHouse() {
        return roomSCommercialHouse;
    }

    public void setRoomSCommercialHouse(String roomSCommercialHouse) {
        this.roomSCommercialHouse = roomSCommercialHouse;
    }

    public String getRoomHouseType() {
        return roomHouseType;
    }

    public void setRoomHouseType(String roomHouseType) {
        this.roomHouseType = roomHouseType;
    }

    @Override
    public String toString() {
        return "ZyRoomDto{" +
                "roomId=" + roomId +
                ", communityId=" + communityId +
                ", communityName='" + communityName + '\'' +
                ", buildingId=" + buildingId +
                ", buildingName='" + buildingName + '\'' +
                ", unitId=" + unitId +
                ", unitName=" + unitName +
                ", roomLevel=" + roomLevel +
                ", roomCode='" + roomCode + '\'' +
                ", roomName='" + roomName + '\'' +
                ", roomAcreage=" + roomAcreage +
                ", roomCost=" + roomCost +
                ", roomStatus='" + roomStatus + '\'' +
                ", roomIsShop='" + roomIsShop + '\'' +
                ", roomSCommercialHouse='" + roomSCommercialHouse + '\'' +
                ", roomHouseType='" + roomHouseType + '\'' +
                '}';
    }
}
