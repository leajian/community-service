package com.zy.community.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyCommunity;
import com.zy.community.community.domain.dto.ZyCommunityDto;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author yangdi
 */
public interface ZyCommunityMapper extends BaseMapper<ZyCommunity> {
    /**
     * 查询列表
     * @param zyCommunity
     * @return
     */
    @Select("<script>" +
            "SELECT " +
            "d.community_id,d.community_name,d.community_code," +
            "d.community_provence_code,d.community_city_code,d.community_town_code," +
            "d.community_detailed_address,d.community_longitude,d.community_latitude," +
            "d.dept_id,d.community_sort,d.create_by," +
            "d.create_time,d.update_by,d.update_time," +
            "d.remark,a.name as communityProvenceName,b.name as communityCityName,c.name as communityTownName FROM zy_community d " +
            "LEFT JOIN sys_area a on d.community_provence_code = a.`code` " +
            "LEFT JOIN sys_area b on d.community_city_code = b.`code`" +
            "LEFT JOIN sys_area c on d.community_town_code = c.`code`" +
            "<where> " +
            "<if test=\"communityName !=null and communityName != ''\">" +
            "d.community_name like concat('%',#{communityName},'%')" +
            "</if> " +
            "<if test=\"communityCode !=null and communityCode != ''\">" +
            "d.community_code = #{communityCode}" +
            "</if> " +
            "${params.dataScope}" +
            "</where>" +
            "</script>")
    List<ZyCommunityDto> queryList(ZyCommunity zyCommunity);
}
