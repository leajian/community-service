package com.zy.community.community.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyFiles;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 文件管理Mapper接口
 * 
 * @author yin
 * @date 2020-12-17
 */
public interface ZyFilesMapper extends BaseMapper<ZyFiles>
{
    /**根据ID数组查询列表*/
    @Select("<script>" +
            "SELECT " +
            "d.files_id,d.files_url,d.create_time " +
            "FROM zy_files d " +
            "<where>" +
            "d.parent_id = #{parentId} " +
            "</where>" +
            "</script>")
    public List<ZyFiles> selectWyglFilesListParentId(Long parentId);

    int insertFilesBatch(@Param("zyFiles") List<ZyFiles> zyFiles);

}
