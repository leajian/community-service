package com.zy.community.community.service.impl;

import java.util.Arrays;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.annotation.DataScope;
import com.zy.community.common.constant.HttpStatus;
import com.zy.community.common.core.domain.ManageParametersUtils;
import com.zy.community.common.exception.CustomException;
import com.zy.community.common.utils.RequestHeaderUtils;
import com.zy.community.common.utils.ServletUtils;
import com.zy.community.community.domain.ZyUnit;
import com.zy.community.community.domain.dto.ZyUnitDto;
import com.zy.community.community.mapper.ZyUnitMapper;
import com.zy.community.community.service.IZyUnitService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 单元 Service业务层处理
 *
 * @author ruoyi
 * @date 2020-12-11
 */
@Service
public class IZyUnitServiceImpl implements IZyUnitService {
    @Value("${community.value}")
    private String propertyId;

    @Resource
    private ZyUnitMapper zyUnitMapper;

    private static final String CODE_PREFIX = "UNIT_";

    /**
     * 查询单元
     *
     * @param unitId 单元 ID
     * @return 单元
     */
    @Override
    public ZyUnit selectZyUnitById(Long unitId) {
        return zyUnitMapper.selectById(unitId);
    }

    /**
     * 查询单元 列表
     *
     * @param zyUnit 单元
     * @return 单元
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<ZyUnitDto> selectZyUnitList(ZyUnit zyUnit) {
        zyUnit.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));

        return zyUnitMapper.selectZyUnitList(zyUnit);
    }

    /**
     * 新增单元
     *
     * @param zyUnit 单元
     * @return 结果
     */
    @Override
    public int insertZyUnit(ZyUnit zyUnit) {

        zyUnit.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));

        zyUnit.setUnitCode(CODE_PREFIX+System.currentTimeMillis());
        return zyUnitMapper.insert(ManageParametersUtils.saveMethod(zyUnit));
    }

    /**
     * 修改单元
     *
     * @param zyUnit 单元
     * @return 结果
     */
    @Override
    public int updateZyUnit(ZyUnit zyUnit) {
        return zyUnitMapper.updateById(ManageParametersUtils.updateMethod(zyUnit));
    }

    /**
     * 批量删除单元
     *
     * @param unitIds 需要删除的单元 ID
     * @return 结果
     */
    @Override
    public int deleteZyUnitByIds(Long[] unitIds) {
        return zyUnitMapper.deleteBatchIds(Arrays.asList(unitIds));
    }

    /**
     * 删除单元 信息
     *
     * @param unitId 单元 ID
     * @return 结果
     */
    @Override
    public int deleteZyUnitById(Long unitId) {
        return zyUnitMapper.deleteById(unitId);
    }
}
