package com.zy.community.community.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zy.community.community.domain.ZyCommunity;
import com.zy.community.community.domain.dto.ZyCommunityDto;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author yangdi
 */
public interface IZyCommunityService {
    /**
     * 查询小区 列表
     * @param zyCommunity
     * @return
     */
    List<ZyCommunityDto> selectZyCommunityList(ZyCommunity zyCommunity);

    /**
     * 获取小区 详细信息
     * @param communityId
     * @return
     */
    ZyCommunity selectZyCommunityById(Long communityId);

    /**
     * 新增小区
     * @param zyCommunity
     * @return
     */
    int insertZyCommunity(ZyCommunity zyCommunity);

    /**
     * 修改小区
     * @param zyCommunity
     * @return
     */
    int updateZyCommunity(ZyCommunity zyCommunity);

    /**
     * 删除小区
     * @param communityIds
     * @return
     */
    int deleteZyCommunityByIds(Long[] communityIds);
}
