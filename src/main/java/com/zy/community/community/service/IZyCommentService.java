package com.zy.community.community.service;

import com.zy.community.community.domain.ZyComment;
import com.zy.community.community.domain.dto.ZyCommentDto;

import java.util.List;

/**
 * 评论Service接口
 * 
 * @author yin
 * @date 2020-12-18
 */
public interface IZyCommentService 
{
    /**
     * 查询评论
     * 
     * @param commentId 评论ID
     * @return 评论
     */
    public ZyComment selectZyCommentById(Long commentId);

    /**
     * 查询评论列表
     * 
     * @param zyComment 评论
     * @return 评论集合
     */
    public List<ZyCommentDto> selectZyCommentList(ZyCommentDto zyComment);

    /**
     * 新增评论
     * 
     * @param zyComment 评论
     * @return 结果
     */
    public int insertZyComment(ZyComment zyComment);

    /**
     * 修改评论
     * 
     * @param zyComment 评论
     * @return 结果
     */
    public int updateZyComment(ZyComment zyComment);

    /**
     * 批量删除评论
     * 
     * @param commentIds 需要删除的评论ID
     * @return 结果
     */
    public int deleteZyCommentByIds(Long[] commentIds);

    /**
     * 删除评论信息
     * 
     * @param commentId 评论ID
     * @return 结果
     */
    public int deleteZyCommentById(Long commentId);
}
