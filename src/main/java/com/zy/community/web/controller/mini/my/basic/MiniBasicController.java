package com.zy.community.web.controller.mini.my.basic;

import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.mini.service.my.basic.MiniBasicService;
import com.zy.community.web.controller.mini.login.dto.MiniUserDto;
import com.zy.community.web.controller.mini.my.basic.dto.UserBasicDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 修改基本信息
 */
@RestController
@RequestMapping("/mini/community/basic")
public class MiniBasicController {
    @Resource
    private MiniBasicService miniBasicService;

    @PostMapping("/edit")
    public ZyResult<MiniUserDto> editBasicInfo(@RequestBody UserBasicDto userBasicDto){
        return miniBasicService.editBasicInfo(userBasicDto);
    }
}
