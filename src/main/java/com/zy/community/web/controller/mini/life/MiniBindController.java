package com.zy.community.web.controller.mini.life;

import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.mini.service.life.wyservice.MiniBindService;
import com.zy.community.web.controller.mini.life.dto.BindDto;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 绑定的Api
 */
@RestController
@RequestMapping("/mini/community/bind")
public class MiniBindController {
    @Resource
    private MiniBindService miniBindService;

    /**
     * 绑定
     * @param dto 绑定提交信息
     * @param result 绑定结果
     * @return 绑定结果
     */
    @PostMapping("/room")
    public ZyResult<String> bindRoomInfo(@RequestBody @Valid BindDto dto, BindingResult result){
        if (result.hasErrors()){
            return ZyResult.fail(400,result.getFieldError().getDefaultMessage());
        }
        return miniBindService.bindRoom(dto);
    }


}
