package com.zy.community.web.controller.community;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.common.utils.poi.ExcelUtil;
import com.zy.community.community.domain.ZyCommunityInteraction;
import com.zy.community.community.domain.dto.ZyCommunityInteractionDto;
import com.zy.community.community.service.impl.ZyCommunityInteractionServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 社区互动Controller
 * 
 * @author yin
 * @date 2020-12-17
 */
@Api(tags = "移动端社区互动")
@RestController
@RequestMapping("/mini/interaction")
public class ZyCommunityInteractionController extends BaseController
{
    @Autowired
    private ZyCommunityInteractionServiceImpl zyCommunityInteractionService;

    /**
     * 查询社区互动列表
     */
    @ApiOperation(value = "移动端查询社区互动列表")
    @PreAuthorize("@ss.hasPermi('system:interaction:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyCommunityInteractionDto zyCommunityInteraction)
    {
        startPage();
        List<ZyCommunityInteractionDto> list = zyCommunityInteractionService.selectZyCommunityInteractionList(zyCommunityInteraction);
        return getDataTable(list);
    }

    /**
     * 导出社区互动列表
     */
    @ApiOperation(value = "移动端导出社区互动列表")
    @PreAuthorize("@ss.hasPermi('system:interaction:export')")
    @Log(title = "社区互动", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public ZyResult export(ZyCommunityInteractionDto zyCommunityInteraction)
    {
        List<ZyCommunityInteractionDto> list = zyCommunityInteractionService.selectZyCommunityInteractionList(zyCommunityInteraction);
        ExcelUtil<ZyCommunityInteractionDto> util = new ExcelUtil<ZyCommunityInteractionDto>(ZyCommunityInteractionDto.class);
        return util.exportExcel(list, "interaction");
    }

    /**
     * 获取社区互动详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:interaction:query')")
    @GetMapping(value = "/{interactionId}")
    public ZyResult getInfo(@PathVariable("interactionId") Long interactionId)
    {
        return ZyResult.data(zyCommunityInteractionService.selectZyCommunityInteractionById(interactionId));
    }

    /**
     * 新增社区互动
     */
    @ApiOperation(value = "移动端新增社区互动")
    @PreAuthorize("@ss.hasPermi('system:interaction:add')")
    @Log(title = "社区互动", businessType = BusinessType.INSERT)
    @PostMapping
    public ZyResult add(@RequestBody ZyCommunityInteractionDto zyCommunityInteraction)
    {
        return toZyAjax(zyCommunityInteractionService.insertZyCommunityInteraction(zyCommunityInteraction));
    }

    /**
     * 修改社区互动
     */
    @ApiOperation(value = "移动端修改社区互动")
    @PreAuthorize("@ss.hasPermi('system:interaction:edit')")
    @Log(title = "社区互动", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult edit(@RequestBody ZyCommunityInteraction zyCommunityInteraction)
    {
        return toZyAjax(zyCommunityInteractionService.updateZyCommunityInteraction(zyCommunityInteraction));
    }

    /**
     * 删除社区互动
     */
    @ApiOperation(value = "移动端删除社区互动")
    @PreAuthorize("@ss.hasPermi('system:interaction:remove')")
    @Log(title = "社区互动", businessType = BusinessType.DELETE)
	@DeleteMapping("/{interactionIds}")
    public ZyResult remove(@PathVariable Long[] interactionIds)
    {
        return toZyAjax(zyCommunityInteractionService.deleteZyCommunityInteractionByIds(interactionIds));
    }
}
