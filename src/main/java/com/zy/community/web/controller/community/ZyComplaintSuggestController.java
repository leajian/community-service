package com.zy.community.web.controller.community;

import java.util.List;

import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.community.domain.ZyComplaintSuggest;
import com.zy.community.community.domain.dto.ZyComplaintSuggestDto;
import com.zy.community.community.service.IZyComplaintSuggestService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 投诉建议 Controller
 * 
 * @author yangdi
 * @date 2020-12-18
 */
@RestController
@RequestMapping("/system/suggest")
public class ZyComplaintSuggestController extends BaseController
{
    @Resource
    private IZyComplaintSuggestService zyComplaintSuggestService;

    /**
     * 查询投诉建议 列表
     */
    @PreAuthorize("@ss.hasPermi('system:suggest:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyComplaintSuggest zyComplaintSuggest)
    {
        startPage();
        List<ZyComplaintSuggestDto> list = zyComplaintSuggestService.selectZyComplaintSuggestList(zyComplaintSuggest);
        return getDataTable(list);
    }

}
