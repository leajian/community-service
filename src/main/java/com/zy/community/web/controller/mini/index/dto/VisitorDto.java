package com.zy.community.web.controller.mini.index.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class VisitorDto implements Serializable {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long visitorId;

    @NotBlank(message = "访客姓名不能为空")
    private String visitorName;
    @NotBlank(message = "访客手机号不能为空")
    private String visitorPhone;
    @NotNull(message = "到访日期不能为空")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date visitorDate;

    @NotNull(message = "社区Id不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public String getVisitorPhone() {
        return visitorPhone;
    }

    public void setVisitorPhone(String visitorPhone) {
        this.visitorPhone = visitorPhone;
    }

    public Date getVisitorDate() {
        return visitorDate;
    }

    public void setVisitorDate(Date visitorDate) {
        this.visitorDate = visitorDate;
    }

    public Long getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(Long visitorId) {
        this.visitorId = visitorId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }
}
