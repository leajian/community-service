package com.zy.community.web.controller.mini.index;

import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.mini.service.index.MiniCardService;
import com.zy.community.web.controller.mini.index.dto.CardDto;
import com.zy.community.web.controller.mini.index.dto.ChangeIdentifyDto;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 身份信息
 */
@RestController
@RequestMapping("/mini/community/card")
public class MiniCardController {
    @Resource
    private MiniCardService miniCardService;

    /**
     * 获取身份信息
     *
     * @param communityId 社区信息
     * @return 身份信息
     */
    @GetMapping("/info/{communityId}")
    public ZyResult<List<CardDto>> findOwnerCardInfo(@PathVariable("communityId") Long communityId) {
        return miniCardService.findCurrentOwnerCardInfo(communityId);
    }

    /**
     * 身份变更申请
     *
     * @param changeIdentifyDto 申请数据
     * @return 申请结果
     */
    @PostMapping("/change")
    public ZyResult<String> changeIdentifyOption(@RequestBody ChangeIdentifyDto changeIdentifyDto) {
        return miniCardService.changeIdentify(changeIdentifyDto);
    }
}
