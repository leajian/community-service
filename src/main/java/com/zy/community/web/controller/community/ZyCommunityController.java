package com.zy.community.web.controller.community;

import java.util.List;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.common.utils.poi.ExcelUtil;
import com.zy.community.community.domain.ZyCommunity;
import com.zy.community.community.domain.dto.ZyCommunityDto;
import com.zy.community.community.service.IZyCommunityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 小区 Controller
 *
 * @author ruoyi
 * @date 2020-12-10
 */
@Api(tags = "小区")
@RestController
@RequestMapping("/community/community")
public class ZyCommunityController extends BaseController {
    @Resource
    private IZyCommunityService IZyCommunityService;

    /**
     * 查询小区 列表
     */
    @ApiOperation(value = "查询小区")
    @PreAuthorize("@ss.hasPermi('system:community:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyCommunity zyCommunity) {
        startPage();
        List<ZyCommunityDto> list = IZyCommunityService.selectZyCommunityList(zyCommunity);
        return getDataTable(list);
    }

    /**
     * 头部下拉 列表
     */
    @ApiOperation(value = "头部下拉")
    @GetMapping("/queryPullDown")
    public ZyResult queryPullDown(ZyCommunity zyCommunity) {
        List<ZyCommunityDto> list = IZyCommunityService.selectZyCommunityList(zyCommunity);
        return ZyResult.data(list);
    }

    /**
     * 导出小区 列表
     */
    @ApiOperation(value = "导出小区")
    @PreAuthorize("@ss.hasPermi('system:community:export')")
    @Log(title = "小区 ", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public ZyResult export(ZyCommunity zyCommunity) {
        List<ZyCommunityDto> list = IZyCommunityService.selectZyCommunityList(zyCommunity);
        ExcelUtil<ZyCommunityDto> util = new ExcelUtil<ZyCommunityDto>(ZyCommunityDto.class);
        return util.exportExcel(list, "community");
    }

    /**
     * 获取小区 详细信息
     */
    @ApiOperation(value = "获取小区")
    @PreAuthorize("@ss.hasPermi('system:community:query')")
    @GetMapping(value = "/{communityId}")
    public ZyResult getInfo(@PathVariable("communityId") Long communityId) {
        return ZyResult.data(IZyCommunityService.selectZyCommunityById(communityId));
    }

    /**
     * 新增小区
     */
    @ApiOperation(value = "新增小区")
    @PreAuthorize("@ss.hasPermi('system:community:add')")
    @Log(title = "小区 ", businessType = BusinessType.INSERT)
    @PostMapping
    public ZyResult add(@RequestBody ZyCommunity zyCommunity) {
        return toAjax(IZyCommunityService.insertZyCommunity(zyCommunity));
    }

    /**
     * 修改小区
     */
    @ApiOperation(value = "修改小区")
    @PreAuthorize("@ss.hasPermi('system:community:edit')")
    @Log(title = "小区 ", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult edit(@RequestBody ZyCommunity zyCommunity) {
        return toAjax(IZyCommunityService.updateZyCommunity(zyCommunity));
    }

    /**
     * 删除小区
     */
    @ApiOperation(value = "删除小区")
    @PreAuthorize("@ss.hasPermi('system:community:remove')")
    @Log(title = "小区 ", businessType = BusinessType.DELETE)
    @DeleteMapping("/{communityIds}")
    public ZyResult remove(@PathVariable Long[] communityIds) {
        return toAjax(IZyCommunityService.deleteZyCommunityByIds(communityIds));
    }

    /**
     * 更换物业
     */
    @ApiOperation(value = "更换物业")
    @PreAuthorize("@ss.hasPermi('system:community:change')")
    @Log(title = "小区 ", businessType = BusinessType.UPDATE)
    @PutMapping("/changeDept")
    public ZyResult changeDept(@RequestBody ZyCommunity zyCommunity) {
        return toAjax(IZyCommunityService.updateZyCommunity(zyCommunity));
    }

}
