package com.zy.community.mini.service.life.wyservice;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.community.domain.ZyOwner;
import com.zy.community.community.domain.ZyOwnerRoom;
import com.zy.community.community.domain.ZyOwnerRoomRecord;
import com.zy.community.community.domain.vo.RoomStatus;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.community.mapper.ZyOwnerRoomMapper;
import com.zy.community.community.mapper.ZyOwnerRoomRecordMapper;
import com.zy.community.framework.security.mini.MiniContextUtils;
import com.zy.community.mini.factory.BindFactory;
import com.zy.community.web.controller.mini.life.dto.BindCommonDto;
import com.zy.community.web.controller.mini.life.dto.BindDto;
import com.zy.community.web.controller.mini.life.dto.BindResultDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * 房屋绑定服务
 */
@Service
public class MiniBindService {
    @Resource
    private ZyOwnerRoomMapper zyOwnerRoomMapper;
    @Resource
    private ZyOwnerRoomRecordMapper zyOwnerRoomRecordMapper;
    @Resource
    private BindFactory bindFactory;
    @Resource
    private ZyOwnerMapper zyOwnerMapper;

    /**
     * 房屋绑定
     * 事务的隔离级别控制在读未提交,可以使得事务中锁有作用
     *
     * @param bindDto 绑定信息
     * @return 绑定结果
     */
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public ZyResult<String> bindRoom(BindDto bindDto) {
        String openId = MiniContextUtils.getOpenId();
        if (StringUtils.isEmpty(openId)) return ZyResult.fail("用户未绑定");
        ZyOwner zyOwner = zyOwnerMapper.selectOne(new QueryWrapper<ZyOwner>().eq("owner_open_id", openId));
        if (zyOwner == null) return ZyResult.fail(404, "该用户不存在");
        //校验是否绑定过
        ZyOwnerRoom zyOwnerRoom = zyOwnerRoomMapper.selectOne(new QueryWrapper<ZyOwnerRoom>().eq("room_id", bindDto.getRoomId())
                .eq("owner_id", zyOwner.getOwnerId()).not(wrapper -> wrapper.eq("room_status", RoomStatus.Reject.name())));
        if (zyOwnerRoom != null) return ZyResult.fail(500, "已经绑定过,请勿重新绑定");

        //查看是否是业主绑定
        bindDto.setOwnerId(zyOwner.getOwnerId());
        ZyOwnerRoom bindInfo = bindFactory.createBindInfo(bindDto);
        if (bindDto.getOwnerType().equals("yz")) {
            synchronized (this) {
                //如果是业主绑定,那么查看该房屋是否已经绑定了业主
                ZyOwnerRoom zyOwnerRoom1 = zyOwnerRoomMapper.selectOne(new QueryWrapper<ZyOwnerRoom>()
                        .eq("room_id", bindDto.getRoomId())
                        .eq("owner_type", "yz")
                        .eq("room_status", RoomStatus.Binding.name()));
                if (zyOwnerRoom1 != null) return ZyResult.fail(500, "该房屋业主已经绑定,请绑定其他类型");
                zyOwnerRoomMapper.insert(bindInfo);
            }
        } else {

            zyOwnerRoomMapper.insert(bindInfo);
        }
        ZyOwnerRoomRecord bindRecord = bindFactory.createBindRecord(bindInfo);
        zyOwnerRoomRecordMapper.insert(bindRecord);
        return ZyResult.success("绑定成功!");
    }


}
