package com.zy.community.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zy.community.common.utils.DateUtils;
import com.zy.community.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author yangdi
 */
@Slf4j
//@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        this.strictInsertFill(metaObject, "setCreateBy", String.class, SecurityUtils.getUsername());
        this.strictInsertFill(metaObject, "setUpdateBy", String.class, SecurityUtils.getUsername());
        this.strictInsertFill(metaObject, "setCreateTime", Date.class, DateUtils.getNowDate());
        this.strictInsertFill(metaObject, "setUpdateTime", Date.class, DateUtils.getNowDate());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
    }
}
